# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rango', '0003_auto_20150708_0348'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='slug_url',
            field=models.CharField(max_length=123, default='hello'),
            preserve_default=True,
        ),
    ]
