from django.shortcuts import render
from datetime import datetime
# Create your views here.

from django.http import HttpResponseRedirect, HttpResponse


#import the Category model
from rango.models import Category

#import the Page model
from rango.models import Page

from django.contrib.auth import authenticate, login, logout

# @login_required comes from here
from django.contrib.auth.decorators import login_required

def index(request):

    category_list = Category.objects.order_by('-likes')[:5]
    page_list = Page.objects.order_by('-views')[:5]

    context_dict = {'categories': category_list, 'pages': page_list}

    visits = request.session.get('visits')
    if not visits:
        visits = 1
    reset_last_visit_time = False

    last_visit = request.session.get('last_visit')
    if last_visit:
        last_visit_time = datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")

        if (datetime.now() - last_visit_time).seconds > 0:
            # ...reassign the value of the cookie to +1 of what it was before...
            visits = visits + 1
            # ...and update the last visit cookie, too.
            reset_last_visit_time = True
    else:
        # Cookie last_visit doesn't exist, so create it to the current date/time.
        reset_last_visit_time = True

    if reset_last_visit_time:
        request.session['last_visit'] = str(datetime.now())
        request.session['visits'] = visits
    context_dict['visits'] = visits


    response = render(request,'rango/index.html', context_dict)

    return response


def about(request):
	visits = request.session.get('visits')
	context_dict = {'visits':visits}
	return render(request, 'rango/about.html', context_dict)

def category(request, category_name_slug):
	context_dict = {}
	try:
		category = Category.objects.get(slug=category_name_slug)
		context_dict['category_name'] = category.name

		pages = Page.objects.filter(category=category)

		context_dict['pages'] = pages

		context_dict['category'] = category
	except Category.DoesNotExist:
		pass

	return render(request, 'rango/category.html', context_dict)


from rango.forms import CategoryForm

@login_required
def add_category(request):
	#A HTTP POST
	context_dict = {}

	if request.method == 'POST':
		form = CategoryForm(request.POST)

		if form.is_valid():

			form.save(commit=True)

			context_dict['form_success'] = True
		else:
			print(form.errors)
	
	form = CategoryForm()
	context_dict['form'] = form
	
	return render(request, 'rango/add_category.html', context_dict)

from rango.forms import PageForm

@login_required
def add_page(request, category_name_slug):
	context_dict = {}
	try:
		cat = Category.objects.get(slug=category_name_slug)
	except Category.DoesNotExist:
		cat = None

	if request.method == 'POST':
		form = PageForm(request.POST)

		if form.is_valid():
			if cat:
				page = form.save(commit=False)
				page.category = cat
				page.views = 0
				page.save()
				context_dict['form_success'] = True
		else:
			print(form.errors)
	
	form = PageForm()
	context_dict['form'] = form
	context_dict['category'] = cat

	return render(request, 'rango/add_page.html', context_dict)


from rango.forms import UserForm, UserProfileForm

def register(request):

	registered = False

	if request.method == 'POST':
		user_form = UserForm(data=request.POST)
		profile_form = UserProfileForm(data=request.POST)

		if user_form.is_valid() and profile_form.is_valid():
			user = user_form.save()
			user.set_password(user.password)
			user.save()

			profile = profile_form.save(commit=False)
			profile.user = user

			if 'picture' in request.FILES:
				profile.picture = request.FILES['picture']

			profile.save()

			registered = True
		else:
			print(user_form.errors, profile_form.errors)

	else:
		user_form = UserForm()
		profile_form = UserProfileForm()

	context_dict = {'user_form':user_form, 'profile_form':profile_form, 'registered':registered}
	
	return render(request, 'rango/register.html', context_dict)

def user_login(request):

	if request.method == 'POST':

		username = request.POST.get('username')
		password = request.POST.get('password')

		user = authenticate(username=username, password=password)

		if user:
			if user.is_active:
				login(request, user)
				return HttpResponseRedirect('/rango/')
			else:
				return HttpResponse("Your rango account is currently disabled")
		else:
			print("Invalid login details : {0}, {1}".format(username, password))
			return HttpResponse("Invalide login details supplied.")
	else:
		return render(request, 'rango/login.html', {})

@login_required
def restricted(request):
	return HttpResponse("You are logged in, so you can see this text!")

@login_required
def user_logout(request):

	logout(request)

	return HttpResponseRedirect('/rango/')

@login_required
def like_category(request):
	cat_id = None

	if request.method == 'GET':
		cat_id = request.GET['category_id']

	likes = 0

	if cat_id:
		cat = Category.objects.get(id=int(cat_id))
		if cat:
			likes = cat.likes + 1
			cat.likes = likes
			cat.save()
	return HttpResponse(likes)