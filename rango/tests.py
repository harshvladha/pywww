from django.test import TestCase

# Create your tests here.
from rango.models import Category
from time import sleep

class CategoryMethodTests(TestCase):
	def test_ensure_views_are_positive(self):
		'''
			 ensure_views_are_positive should results True for categories where views are zero or positive
		'''
		cat = Category(name='test', views=-1, likes=0)
		cat.save()
		self.assertEqual((cat.views >= 0), True)

	def test_slug_line_creation(self):

		cat = Category(name="Rango Category String")
		cat.save()
		self.assertEqual(cat.slug, 'rango-category-string')


from django.core.urlresolvers import reverse


class IndexViewTests(TestCase):

    def test_index_view_with_no_categories(self):
        """
        If no questions exist, an appropriate message should be displayed.
        """
        Category.objects.get_or_create(name='test', views=1, likes=1)
        Category.objects.get_or_create(name='temp', views=1, likes=1)
        Category.objects.get_or_create(name='tmp', views=1, likes=1)
        Category.objects.get_or_create(name='tmp test temp', views=1, likes=1)

        response = self.client.get(reverse('index'))
        
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "tmp test temp")
        
        num_cat = len(response.context['categories'])
        self.assertEqual(num_cat, 4)