from django.db import models

# Create your models here.


from django.template.defaultfilters import slugify
#from django.core.validators import MinValueValidtor

class Category(models.Model):
	name = models.CharField(max_length=128, unique=True)
	views = models.PositiveIntegerField(default=0)
	likes = models.IntegerField(default=0)
	slug = models.SlugField()
	
	def save(self, *args, **kwargs):
		if self.views < 0:
			self.views = 0
		self.slug = slugify(self.name)
		super(Category, self).save(*args, **kwargs)

	
	def __str__(self):
		return self.name

	class Meta:
		verbose_name_plural = "Categories"

class Page(models.Model):
	category = models.ForeignKey(Category)
	title =  models.CharField(max_length=128)
	url = models.URLField()
	views = models.IntegerField(default=0)

	def __str__(self):
		return self.title

from django.contrib.auth.models import User

class UserProfile(models.Model):
	# This line is required. Links UserProfile to a User model instance.
	user = models.OneToOneField(User)

	website = models.URLField(blank=True)
	picture = models.ImageField(upload_to='profile_images', blank=True)

	def __str__(self):
		return self.user.username
